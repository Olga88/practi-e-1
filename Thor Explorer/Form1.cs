﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using System.Threading;

namespace Thor_Explorer
{
    public partial class mainForm : Form
    {

        public mainForm()
        {
            InitializeComponent();
        }

        [DllImport("uxtheme.dll", CharSet = CharSet.Unicode, EntryPoint = "SetWindowTheme")]
        public static extern int SetWindowTheme(IntPtr hwnd, string pszSubAppName, string pszSubIdList);
        private void mainForm_Load(object sender, EventArgs e)
        {
            SetWindowTheme(treeView1.Handle, "explorer", null);
        }

        private void toolExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        OpenFileDialog ofd = new();
        string line = "";
        private void tooLoadLog_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                while (line != null)
                {
                    line = sr.ReadLine();
                    if (line != null)
                    {
                        DateTime dateTimeResult;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-EN");
                        string[] subLine = line.Split(new string[] { ":[", "][", "] :" }, StringSplitOptions.RemoveEmptyEntries);
                        //с этого момента нужно создавать отдельный метод, куда скидывать полученный массив subLine
                        //и уже в нем обрабатывать полученные данные
                        string dateTimeString = subLine[1];
                        string format1 = "yyyy/MM/dd HH:mm:ss.ffff";
                        dateTimeResult = DateTime.ParseExact(dateTimeString, format1, provider);
                        ListViewItem nextRow = new ListViewItem(subLine[0].Trim(' '));
                        if (subLine[0].Trim(' ') == "INFO")
                        {
                            nextRow.BackColor = Color.LightGreen;
                        }
                        else if (subLine[0].Trim(' ') == "ERROR")
                        {
                            nextRow.BackColor = Color.Red;
                        }
                        else if (subLine[0].Trim(' ') == "WARNING")
                        {
                            nextRow.BackColor = Color.Orange;
                        }
                        else if (subLine[0].Trim(' ') == "EXCEPTION")
                        {
                            nextRow.BackColor = Color.LightBlue;
                        }
                        else
                        {
                            nextRow.BackColor = Color.Yellow;
                        }
                        ListViewItem.ListViewSubItem sharp = new ListViewItem.ListViewSubItem(nextRow, "");
                        ListViewItem.ListViewSubItem date = new ListViewItem.ListViewSubItem(nextRow, dateTimeResult.ToShortDateString());
                        ListViewItem.ListViewSubItem time = new ListViewItem.ListViewSubItem(nextRow, dateTimeResult.ToLongTimeString());
                        ListViewItem.ListViewSubItem thread = new ListViewItem.ListViewSubItem(nextRow, subLine[2].Trim(' '));
                        ListViewItem.ListViewSubItem msg = new ListViewItem.ListViewSubItem(nextRow, subLine[3].Trim(' '));
                        nextRow.SubItems.Add(sharp);
                        nextRow.SubItems.Add(date);
                        nextRow.SubItems.Add(time);
                        nextRow.SubItems.Add(thread);
                        nextRow.SubItems.Add(msg);
                        listView1.Items.Add(nextRow);
                    }
                }
                //listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                sr.Close();
            }
        }

        private void toolLoadTrades_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                DataTable dataTable = new DataTable();

                string[] fileContent = File.ReadAllLines(ofd.FileName);
                string[] columns = { "Order#", "Product", "Side", "Quintity", "Price", "Time", " " };
                if (fileContent.Count() > 0)
                {
                    
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-EN");
                    for (int i = 0; i < (columns.Length); i++)
                    {
                        dataTable.Columns.Add(columns[i]);
                        
                    }
                    for (int i = 0; i < fileContent.Count(); i++)
                    {
                        
                        string[] rowData = fileContent[i].Split(',');
                        if ((rowData.Length > 5) && (!string.IsNullOrEmpty(rowData[5])))
                        {
                            string dateTimeString = rowData[5];
                            Console.WriteLine(dateTimeString);
                            string format = "yyyy/MM/dd HH:mm:ss.ffff";
                            DateTime dateTimeResult = DateTime.ParseExact(dateTimeString, format, provider);
                            rowData[5] = dateTimeResult.ToString();
                        }
                        dataTable.Rows.Add(rowData);
                    }
                    
                }
                dataGridView1.DataSource = dataTable;
            }
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void dataGridView1_Resize(object sender, EventArgs e)
        {
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
    }
}
