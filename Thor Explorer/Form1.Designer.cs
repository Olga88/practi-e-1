﻿
namespace Thor_Explorer
{
    partial class mainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Global");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("SecurityEngine", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("OdbEngine");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("TdbEngine");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("ThorTableEngine");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Execution Engine");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("RemoteLogEngine");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("WS64-NEW.Thor", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ColumnOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnQuintity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTime2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.toolFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTest = new System.Windows.Forms.ToolStripMenuItem();
            this.tooLoadLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolLoadTrades = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.tabControlServer = new System.Windows.Forms.TabControl();
            this.tabServer = new System.Windows.Forms.TabPage();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageListServer = new System.Windows.Forms.ImageList(this.components);
            this.buttonApply = new System.Windows.Forms.Button();
            this.tabMacros = new System.Windows.Forms.TabPage();
            this.splitContainerInserted = new System.Windows.Forms.SplitContainer();
            this.tabControlSystem = new System.Windows.Forms.TabControl();
            this.tabSystemLog = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.checkVerbose = new System.Windows.Forms.CheckBox();
            this.checkInfo = new System.Windows.Forms.CheckBox();
            this.checkWarnings = new System.Windows.Forms.CheckBox();
            this.checkErrors = new System.Windows.Forms.CheckBox();
            this.checkExseption = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnType = new System.Windows.Forms.ColumnHeader();
            this.columnSharp = new System.Windows.Forms.ColumnHeader();
            this.columnDate = new System.Windows.Forms.ColumnHeader();
            this.columnTime = new System.Windows.Forms.ColumnHeader();
            this.columnThread = new System.Windows.Forms.ColumnHeader();
            this.columnMessage = new System.Windows.Forms.ColumnHeader();
            this.tabConsole = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menu.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
            this.splitContainerMain.Panel1.SuspendLayout();
            this.splitContainerMain.Panel2.SuspendLayout();
            this.splitContainerMain.SuspendLayout();
            this.tabControlServer.SuspendLayout();
            this.tabServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerInserted)).BeginInit();
            this.splitContainerInserted.Panel1.SuspendLayout();
            this.splitContainerInserted.Panel2.SuspendLayout();
            this.splitContainerInserted.SuspendLayout();
            this.tabControlSystem.SuspendLayout();
            this.tabSystemLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnOrder,
            this.ColumnProduct,
            this.ColumnSide,
            this.ColumnQuintity,
            this.ColumnPrice,
            this.ColumnTime2,
            this.Column1});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.Size = new System.Drawing.Size(779, 215);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Resize += new System.EventHandler(this.dataGridView1_Resize);
            // 
            // ColumnOrder
            // 
            this.ColumnOrder.DataPropertyName = "Order#";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnOrder.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnOrder.HeaderText = "Order#";
            this.ColumnOrder.MinimumWidth = 120;
            this.ColumnOrder.Name = "ColumnOrder";
            this.ColumnOrder.ReadOnly = true;
            // 
            // ColumnProduct
            // 
            this.ColumnProduct.DataPropertyName = "Product";
            this.ColumnProduct.HeaderText = "Product";
            this.ColumnProduct.MinimumWidth = 120;
            this.ColumnProduct.Name = "ColumnProduct";
            this.ColumnProduct.ReadOnly = true;
            // 
            // ColumnSide
            // 
            this.ColumnSide.DataPropertyName = "Side";
            this.ColumnSide.HeaderText = "Side";
            this.ColumnSide.MinimumWidth = 120;
            this.ColumnSide.Name = "ColumnSide";
            this.ColumnSide.ReadOnly = true;
            // 
            // ColumnQuintity
            // 
            this.ColumnQuintity.DataPropertyName = "Quintity";
            this.ColumnQuintity.HeaderText = "Quintity";
            this.ColumnQuintity.MinimumWidth = 120;
            this.ColumnQuintity.Name = "ColumnQuintity";
            this.ColumnQuintity.ReadOnly = true;
            // 
            // ColumnPrice
            // 
            this.ColumnPrice.DataPropertyName = "Price";
            this.ColumnPrice.HeaderText = "Price";
            this.ColumnPrice.MinimumWidth = 120;
            this.ColumnPrice.Name = "ColumnPrice";
            this.ColumnPrice.ReadOnly = true;
            // 
            // ColumnTime2
            // 
            this.ColumnTime2.DataPropertyName = "Time";
            this.ColumnTime2.HeaderText = "Time";
            this.ColumnTime2.MinimumWidth = 120;
            this.ColumnTime2.Name = "ColumnTime2";
            this.ColumnTime2.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = " ";
            this.Column1.HeaderText = "";
            this.Column1.MinimumWidth = 120;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFile,
            this.toolTest});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1082, 28);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // toolFile
            // 
            this.toolFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolSave,
            this.toolExit});
            this.toolFile.Name = "toolFile";
            this.toolFile.Size = new System.Drawing.Size(46, 24);
            this.toolFile.Text = "&File";
            // 
            // toolSave
            // 
            this.toolSave.Name = "toolSave";
            this.toolSave.Size = new System.Drawing.Size(123, 26);
            this.toolSave.Text = "Save";
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(123, 26);
            this.toolExit.Text = "Exit";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // toolTest
            // 
            this.toolTest.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tooLoadLog,
            this.toolLoadTrades});
            this.toolTest.Name = "toolTest";
            this.toolTest.Size = new System.Drawing.Size(49, 24);
            this.toolTest.Text = "Test";
            // 
            // tooLoadLog
            // 
            this.tooLoadLog.Name = "tooLoadLog";
            this.tooLoadLog.Size = new System.Drawing.Size(172, 26);
            this.tooLoadLog.Text = "Load Log";
            this.tooLoadLog.Click += new System.EventHandler(this.tooLoadLog_Click);
            // 
            // toolLoadTrades
            // 
            this.toolLoadTrades.Name = "toolLoadTrades";
            this.toolLoadTrades.Size = new System.Drawing.Size(172, 26);
            this.toolLoadTrades.Text = "Load Trades";
            this.toolLoadTrades.Click += new System.EventHandler(this.toolLoadTrades_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1082, 27);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Thor_Explorer.Properties.Resources.server;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::Thor_Explorer.Properties.Resources.contract;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // splitContainerMain
            // 
            this.splitContainerMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerMain.Location = new System.Drawing.Point(0, 55);
            this.splitContainerMain.Name = "splitContainerMain";
            // 
            // splitContainerMain.Panel1
            // 
            this.splitContainerMain.Panel1.Controls.Add(this.tabControlServer);
            // 
            // splitContainerMain.Panel2
            // 
            this.splitContainerMain.Panel2.Controls.Add(this.splitContainerInserted);
            this.splitContainerMain.Size = new System.Drawing.Size(1082, 488);
            this.splitContainerMain.SplitterDistance = 300;
            this.splitContainerMain.SplitterWidth = 1;
            this.splitContainerMain.TabIndex = 2;
            // 
            // tabControlServer
            // 
            this.tabControlServer.Controls.Add(this.tabServer);
            this.tabControlServer.Controls.Add(this.tabMacros);
            this.tabControlServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlServer.Location = new System.Drawing.Point(0, 0);
            this.tabControlServer.Margin = new System.Windows.Forms.Padding(0);
            this.tabControlServer.Name = "tabControlServer";
            this.tabControlServer.SelectedIndex = 0;
            this.tabControlServer.Size = new System.Drawing.Size(298, 486);
            this.tabControlServer.TabIndex = 0;
            // 
            // tabServer
            // 
            this.tabServer.Controls.Add(this.textBoxServer);
            this.tabServer.Controls.Add(this.treeView1);
            this.tabServer.Controls.Add(this.buttonApply);
            this.tabServer.Location = new System.Drawing.Point(4, 29);
            this.tabServer.Name = "tabServer";
            this.tabServer.Padding = new System.Windows.Forms.Padding(3);
            this.tabServer.Size = new System.Drawing.Size(290, 453);
            this.tabServer.TabIndex = 0;
            this.tabServer.Text = "Server";
            this.tabServer.UseVisualStyleBackColor = true;
            // 
            // textBoxServer
            // 
            this.textBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServer.Location = new System.Drawing.Point(9, 7);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(202, 27);
            this.textBoxServer.TabIndex = 3;
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageListServer;
            this.treeView1.Location = new System.Drawing.Point(9, 38);
            this.treeView1.Name = "treeView1";
            treeNode1.ImageIndex = 2;
            treeNode1.Name = "Node2";
            treeNode1.Text = "Global";
            treeNode2.ImageIndex = 1;
            treeNode2.Name = "Node1";
            treeNode2.Text = "SecurityEngine";
            treeNode3.ImageIndex = 1;
            treeNode3.Name = "Node3";
            treeNode3.Text = "OdbEngine";
            treeNode4.ImageIndex = 1;
            treeNode4.Name = "Node4";
            treeNode4.Text = "TdbEngine";
            treeNode5.ImageIndex = 1;
            treeNode5.Name = "Node5";
            treeNode5.Text = "ThorTableEngine";
            treeNode6.ImageKey = "data_lock.ico";
            treeNode6.Name = "Node6";
            treeNode6.Text = "Execution Engine";
            treeNode7.ImageKey = "data_lock.ico";
            treeNode7.Name = "Node7";
            treeNode7.Text = "RemoteLogEngine";
            treeNode8.ImageIndex = 0;
            treeNode8.Name = "Node0";
            treeNode8.Text = "WS64-NEW.Thor";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode8});
            this.treeView1.PathSeparator = "/";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.ShowLines = false;
            this.treeView1.Size = new System.Drawing.Size(269, 407);
            this.treeView1.TabIndex = 2;
            // 
            // imageListServer
            // 
            this.imageListServer.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListServer.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListServer.ImageStream")));
            this.imageListServer.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListServer.Images.SetKeyName(0, "data.ico");
            this.imageListServer.Images.SetKeyName(1, "data_lock.ico");
            this.imageListServer.Images.SetKeyName(2, "index.ico");
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Location = new System.Drawing.Point(217, 7);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(61, 28);
            this.buttonApply.TabIndex = 1;
            this.buttonApply.Text = "&Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            // 
            // tabMacros
            // 
            this.tabMacros.Location = new System.Drawing.Point(4, 29);
            this.tabMacros.Name = "tabMacros";
            this.tabMacros.Padding = new System.Windows.Forms.Padding(3);
            this.tabMacros.Size = new System.Drawing.Size(290, 453);
            this.tabMacros.TabIndex = 1;
            this.tabMacros.Text = "Macros";
            this.tabMacros.UseVisualStyleBackColor = true;
            // 
            // splitContainerInserted
            // 
            this.splitContainerInserted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerInserted.Location = new System.Drawing.Point(0, 0);
            this.splitContainerInserted.Name = "splitContainerInserted";
            this.splitContainerInserted.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerInserted.Panel1
            // 
            this.splitContainerInserted.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainerInserted.Panel2
            // 
            this.splitContainerInserted.Panel2.Controls.Add(this.tabControlSystem);
            this.splitContainerInserted.Size = new System.Drawing.Size(779, 486);
            this.splitContainerInserted.SplitterDistance = 215;
            this.splitContainerInserted.SplitterWidth = 1;
            this.splitContainerInserted.TabIndex = 0;
            // 
            // tabControlSystem
            // 
            this.tabControlSystem.Controls.Add(this.tabSystemLog);
            this.tabControlSystem.Controls.Add(this.tabConsole);
            this.tabControlSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSystem.Location = new System.Drawing.Point(0, 0);
            this.tabControlSystem.Name = "tabControlSystem";
            this.tabControlSystem.SelectedIndex = 0;
            this.tabControlSystem.Size = new System.Drawing.Size(779, 270);
            this.tabControlSystem.TabIndex = 0;
            // 
            // tabSystemLog
            // 
            this.tabSystemLog.Controls.Add(this.label1);
            this.tabSystemLog.Controls.Add(this.checkVerbose);
            this.tabSystemLog.Controls.Add(this.checkInfo);
            this.tabSystemLog.Controls.Add(this.checkWarnings);
            this.tabSystemLog.Controls.Add(this.checkErrors);
            this.tabSystemLog.Controls.Add(this.checkExseption);
            this.tabSystemLog.Controls.Add(this.listView1);
            this.tabSystemLog.Location = new System.Drawing.Point(4, 29);
            this.tabSystemLog.Name = "tabSystemLog";
            this.tabSystemLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystemLog.Size = new System.Drawing.Size(771, 237);
            this.tabSystemLog.TabIndex = 0;
            this.tabSystemLog.Text = "System Log";
            this.tabSystemLog.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(489, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 25);
            this.label1.TabIndex = 6;
            // 
            // checkVerbose
            // 
            this.checkVerbose.AutoSize = true;
            this.checkVerbose.Location = new System.Drawing.Point(360, 26);
            this.checkVerbose.Name = "checkVerbose";
            this.checkVerbose.Size = new System.Drawing.Size(84, 24);
            this.checkVerbose.TabIndex = 5;
            this.checkVerbose.Text = "Verbose";
            this.checkVerbose.UseVisualStyleBackColor = true;
            // 
            // checkInfo
            // 
            this.checkInfo.AutoSize = true;
            this.checkInfo.Location = new System.Drawing.Point(292, 26);
            this.checkInfo.Name = "checkInfo";
            this.checkInfo.Size = new System.Drawing.Size(57, 24);
            this.checkInfo.TabIndex = 4;
            this.checkInfo.Text = "Info";
            this.checkInfo.UseVisualStyleBackColor = true;
            // 
            // checkWarnings
            // 
            this.checkWarnings.AutoSize = true;
            this.checkWarnings.Location = new System.Drawing.Point(189, 26);
            this.checkWarnings.Name = "checkWarnings";
            this.checkWarnings.Size = new System.Drawing.Size(92, 24);
            this.checkWarnings.TabIndex = 3;
            this.checkWarnings.Text = "Warnings";
            this.checkWarnings.UseVisualStyleBackColor = true;
            // 
            // checkErrors
            // 
            this.checkErrors.AutoSize = true;
            this.checkErrors.Location = new System.Drawing.Point(109, 26);
            this.checkErrors.Name = "checkErrors";
            this.checkErrors.Size = new System.Drawing.Size(69, 24);
            this.checkErrors.TabIndex = 2;
            this.checkErrors.Text = "Errors";
            this.checkErrors.UseVisualStyleBackColor = true;
            // 
            // checkExseption
            // 
            this.checkExseption.AutoSize = true;
            this.checkExseption.Location = new System.Drawing.Point(3, 26);
            this.checkExseption.Name = "checkExseption";
            this.checkExseption.Size = new System.Drawing.Size(95, 24);
            this.checkExseption.TabIndex = 1;
            this.checkExseption.Text = "Exseption";
            this.checkExseption.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnType,
            this.columnSharp,
            this.columnDate,
            this.columnTime,
            this.columnThread,
            this.columnMessage});
            this.listView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(3, 65);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(765, 169);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnType
            // 
            this.columnType.Tag = "";
            this.columnType.Text = "Type";
            this.columnType.Width = 100;
            // 
            // columnSharp
            // 
            this.columnSharp.Tag = "";
            this.columnSharp.Text = "#";
            // 
            // columnDate
            // 
            this.columnDate.Text = "Date";
            this.columnDate.Width = 90;
            // 
            // columnTime
            // 
            this.columnTime.Text = "Time";
            this.columnTime.Width = 110;
            // 
            // columnThread
            // 
            this.columnThread.Text = "Thread";
            this.columnThread.Width = 160;
            // 
            // columnMessage
            // 
            this.columnMessage.Text = "Message";
            this.columnMessage.Width = 270;
            // 
            // tabConsole
            // 
            this.tabConsole.Location = new System.Drawing.Point(4, 29);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(771, 237);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1082, 543);
            this.Controls.Add(this.splitContainerMain);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.Name = "mainForm";
            this.Text = "Thor Explorer";
            this.Load += new System.EventHandler(this.mainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainerMain.Panel1.ResumeLayout(false);
            this.splitContainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
            this.splitContainerMain.ResumeLayout(false);
            this.tabControlServer.ResumeLayout(false);
            this.tabServer.ResumeLayout(false);
            this.tabServer.PerformLayout();
            this.splitContainerInserted.Panel1.ResumeLayout(false);
            this.splitContainerInserted.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerInserted)).EndInit();
            this.splitContainerInserted.ResumeLayout(false);
            this.tabControlSystem.ResumeLayout(false);
            this.tabSystemLog.ResumeLayout(false);
            this.tabSystemLog.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem toolFile;
        private System.Windows.Forms.ToolStripMenuItem toolSave;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.SplitContainer splitContainerMain;
        private System.Windows.Forms.TabControl tabControlServer;
        private System.Windows.Forms.TabPage tabServer;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.TabPage tabMacros;
        private System.Windows.Forms.SplitContainer splitContainerInserted;
        private System.Windows.Forms.ImageList imageListServer;
        private System.Windows.Forms.TabControl tabControlSystem;
        private System.Windows.Forms.TabPage tabSystemLog;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.CheckBox checkVerbose;
        private System.Windows.Forms.CheckBox checkWarnings;
        private System.Windows.Forms.CheckBox checkErrors;
        private System.Windows.Forms.CheckBox checkExseption;
        private System.Windows.Forms.CheckBox checkInfo;
        private System.Windows.Forms.ColumnHeader columnType;
        private System.Windows.Forms.ColumnHeader columnSharp;
        private System.Windows.Forms.ColumnHeader columnDate;
        private System.Windows.Forms.ColumnHeader columnTime;
        private System.Windows.Forms.ColumnHeader columnThread;
        private System.Windows.Forms.ColumnHeader columnMessage;
        public System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private System.Windows.Forms.ToolStripMenuItem toolTest;
        private System.Windows.Forms.ToolStripMenuItem tooLoadLog;
        private System.Windows.Forms.ToolStripMenuItem toolLoadTrades;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSide;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnQuintity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTime2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}

